package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by Cannon on 10/9/2017.
 */
@TeleOp(name = "the right stuff")

public class TeleOpM extends LinearOpMode {


    private static DcMotor leftFrontWheel, leftBackWheel, rightFrontWheel, rightBackWheel;


    @Override
    public void runOpMode() {
        leftFrontWheel = hardwareMap.dcMotor.get("fl");
        leftBackWheel = hardwareMap.dcMotor.get("bl");
        rightFrontWheel = hardwareMap.dcMotor.get("fr");
        rightBackWheel = hardwareMap.dcMotor.get("br");
        double threshhold = .2;

        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {


            if(Math.abs(gamepad1.left_stick_y)>threshhold)
            {
                leftFrontWheel.setPower((gamepad1.left_stick_y)/2);
                rightFrontWheel.setPower((gamepad1.left_stick_y)/2);
                leftBackWheel.setPower((-gamepad1.left_stick_y)/2);
                rightBackWheel.setPower((gamepad1.left_stick_y)/2);
            }
            else if(Math.abs(gamepad1.left_stick_x)>threshhold)
            {
                leftFrontWheel.setPower((-gamepad1.left_stick_x)/2);
                rightFrontWheel.setPower((gamepad1.left_stick_x)/2);
                leftBackWheel.setPower((-gamepad1.left_stick_x)/2);
                rightBackWheel.setPower((-gamepad1.left_stick_x)/2);
            }

            else if(Math.abs(gamepad1.right_stick_x)>threshhold)
            {
                leftFrontWheel.setPower((-gamepad1.right_stick_x)/2);
                rightFrontWheel.setPower((gamepad1.right_stick_x)/2);
                leftBackWheel.setPower((gamepad1.right_stick_x)/2);
                rightBackWheel.setPower((gamepad1.right_stick_x)/2);
            }
            else
            {
                leftFrontWheel.setPower(0);
                rightFrontWheel.setPower(0);
                leftBackWheel.setPower(0);
                rightBackWheel.setPower(0);
            }


            telemetry.addData("Status", "Running");
            telemetry.update();

        }
    }
}