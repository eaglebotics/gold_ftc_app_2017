package org.firstinspires.ftc.teamcode;


import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by Cannon on 10/9/2017.
 */
@Autonomous(name = "Basic Auto")

public class BasicAuto extends LinearOpMode {


    private static DcMotor leftFrontWheel, leftBackWheel, rightFrontWheel, rightBackWheel;
    private static Servo leftArm, rightArm;
    private static ColorSensor color;


    @Override
    public void runOpMode() {
        leftFrontWheel = hardwareMap.dcMotor.get("fl");
        leftBackWheel = hardwareMap.dcMotor.get("bl");
        rightFrontWheel = hardwareMap.dcMotor.get("fr");
        rightBackWheel = hardwareMap.dcMotor.get("br");
        leftBackWheel.setDirection(DcMotorSimple.Direction.REVERSE); //zach crossed the wires like an ass
        leftFrontWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);;
        rightFrontWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftArm = hardwareMap.servo.get("leftArm");
        rightArm = hardwareMap.servo.get("rightArm");
        color = hardwareMap.colorSensor.get("color");

        telemetry.addData("Status", "Initialized");
        telemetry.update();
        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            driveForwardDistance(50,100);
            telemetry.addData("Status", "Running");
            telemetry.update();

        }
    }

    public boolean isRed()
    {
        if(color.red()>color.blue()&&color.red()>color.green())
            return true;
        else
            return false;
    }

    public void driveForward(double power)
    {
        leftFrontWheel.setPower(power);
        rightFrontWheel.setPower(power);
        leftBackWheel.setPower(power);
        rightBackWheel.setPower(power);
    }
    public void driveSideways(double power)
    {
        leftFrontWheel.setPower(-power);
        rightFrontWheel.setPower(power);
        leftBackWheel.setPower(-power);
        rightBackWheel.setPower(-power);
    }
    public void rotate(double power)
    {
        leftFrontWheel.setPower(-power);
        rightFrontWheel.setPower(power);
        leftBackWheel.setPower(power);
        rightBackWheel.setPower(power);
    }
    public void stopDriving()
    {
        driveForward(0);
    }
    public void driveForwardDistance(double power, int distance)
    {
        leftBackWheel.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftFrontWheel.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBackWheel.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFrontWheel.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftFrontWheel.setTargetPosition(distance);
        leftBackWheel.setTargetPosition(distance);
        rightFrontWheel.setTargetPosition(distance);
        rightBackWheel.setTargetPosition(distance);

        leftBackWheel.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        leftFrontWheel.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightBackWheel.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        rightFrontWheel.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        driveForward(power);
        while(opModeIsActive() && leftBackWheel.isBusy() && rightFrontWheel.isBusy()){

            telemetry.addData("Path1",  "Running to %7d", distance);
            telemetry.addData("Path2",  "Running at %7d", leftFrontWheel.getCurrentPosition());
            telemetry.update();

            idle();
        }

        stopDriving();
        leftFrontWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);;
        rightFrontWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackWheel.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

}
